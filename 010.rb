=begin
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
=end

# warning: i take ages to run. 

require 'prime'

num = 0
arr = Array.new
while num < 2_000_000
  if num.prime?
    arr << num
  end
  num = num + 1
end
puts arr.sum.to_s
