=begin
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?
=end

require 'prime'

x = 2
cnt = 0
while cnt != 10001
  if x.prime?
    cnt = cnt + 1
  end
  x = x + 1
end
puts (x-1).to_s
