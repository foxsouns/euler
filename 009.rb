=begin
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a2 + b2 = c2
For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
=end

# natural numbers are numbers above 0
# no number can be above 1k if they need to add to 1k
# im going to do this the only way i can: highly inefficiently

# a**2 + b**2 + sqrt(c) = 1000

a = 0
b = 1
c = 1
numfound = false
while not numfound
  a = a + 1
  if a >= b
    a = 0
    b = b + 1
  end
  if b >= c
    b = 1
    c = c + 1
  end
  if a + b + c == 1000
    if a**2 + b**2 == c**2
      numfound = true
    end
  end
end
puts (a * b * c).to_s
