=begin
A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers.
=end
endf = false
num1 = 100
num2 = 100
num3 = 0
answer = 0
while endf == false
 num3 = num1 * num2
 if num3 == num3.to_s.reverse.to_i
  if num3 > answer
    answer = num3
  end
end
 num2 = num2 + 1
 if num2 == 1000
  num2 = 100
  num1 = num1 + 1
 end
 if num1 == 1000
   endf = true
 end
end
puts answer.to_s
